AWSTemplateFormatVersion: "2010-09-09"
Description: Course Project Backend Service. Version 1.0.0
Parameters:
  RootDomainName:
    Description: Domain name for your website (example.com)
    Type: String
    Default: course-project.link
  SubDomainName:
    Description: SubDomain name for your website
    Type: String
  ServiceName:
    Description: Service Name
    Type: String
  ImageTag:
    Description: Image Tag
    Type: String
    Default: latest
  MySqlDb:
    Description: MySQL DB
    Type: String
  MySqlPort:
    Description: MySQL Port
    Type: Number
    Default: 3306
  DesiredCount:
    Type: Number
    Description: Service tasks desired count
    Default: 1
  MinCapacity:
    Type: Number
    Description: Service tasks min capacity
    Default: 1
  MaxCapacity:
    Type: Number
    Description: Service tasks MAX capacity
    Default: 3
  ScaleUpCpuThreshold:
    Type: Number
    Description: Scale-up threshold for CPU
    Default: 70
    MaxValue: 99
    MinValue: 20
  ScaleDownCpuThreshold:
    Type: Number
    Description: Scale-up threshold for CPU
    Default: 20
    MaxValue: 90
    MinValue: 10
  EcsTaskCpuLimit:
    Type: Number
    Description: >-
      ECS Task CPU Limit
      256 (.25 vCPU) - Available memory values: 0.5GB, 1GB, 2GB
      512 (.5 vCPU) - Available memory values: 1GB, 2GB, 3GB, 4GB
      1024 (1 vCPU) - Available memory values: 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB
      2048 (2 vCPU) - Available memory values: Between 4GB and 16GB in 1GB increments
      4096 (4 vCPU) - Available memory values: Between 8GB and 30GB in 1GB increments
    AllowedValues:
      - 32
      - 64
      - 128
      - 256
      - 512
      - 1024
      - 2048
      - 4096
  EcsTaskMemoryLimit:
    Type: String
    Description: >-
      ECS Task Memory Limit
      0.5GB, 1GB, 2GB - Available cpu values: 256 (.25 vCPU)
      1GB, 2GB, 3GB, 4GB - Available cpu values: 512 (.5 vCPU)
      2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB - Available cpu values: 1024 (1 vCPU)
      Between 4GB and 16GB in 1GB increments - Available cpu values: 2048 (2 vCPU)
      Between 8GB and 30GB in 1GB increments - Available cpu values: 4096 (4 vCPU)
    AllowedValues:
      - "0.25GB"
      - "0.5GB"
      - "1GB"
      - "2GB"
      - "4GB"
      - "8GB"
      - "16GB"
  ApplicationPort:
    Type: Number
    Default: 80
    MinValue: 1
    MaxValue: 32000
  SecurityLevel:
    Type: String
    AllowedValues:
      - low
      - medium
      - high
      - impossible

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      -
        Label:
          default: ECS Service Configuration
        Parameters:
          - ServiceName
          - ImageTag
          - ApplicationPort
          - DesiredCount
          - MinCapacity
          - MaxCapacity
          - EcsTaskCpuLimit
          - EcsTaskMemoryLimit
          - ScaleUpCpuThreshold
          - ScaleDownCpuThreshold
      -
        Label:
          default: Environment Vaiables
        Parameters:
          - MySqlDb
          - MySqlPort
          - SecurityLevel
      -
        Label:
          default: DNS Configuration
        Parameters:
          - RootDomainName
          - SubDomainName
    ParameterLabels:
      ServiceName:
        default: Service Name
      ImageTag:
        default: Docker Image Tag
      ApplicationPort:
        default: Application's Port
      EcsTaskCpuLimit:
        default: ECS Task CPU Limit
      EcsTaskMemoryLimit:
        default: ECS Task Memory Limit
      DesiredCount:
        default: ECS Service Desired Count
      MinCapacity:
        default: ECS Service Min Capacity
      MaxCapacity:
        default: ECS Service Max Capacity
      ScaleUpCpuThreshold:
        default: Scale Up CPU Threshold
      ScaleDownCpuThreshold:
        default: Scale Down CPU Threshold
      MySqlDb:
        default: MySQL DB Name
      MySqlPort:
        default: MySQL DB Port
      RootDomainName:
        default: Root Doman Name
      SubDomainName:
        default: Subdomain Name
      SecurityLevel:
        default: DVWA Security Level

Resources:
  LogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Sub ${ServiceName}-course-project-loggroup
      RetentionInDays: 30

  ContainerSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: !Sub ${ServiceName}-course-project-container-sg
      GroupDescription: Container security group
      VpcId:
        Fn::ImportValue: course-project-vpc
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: !Ref ApplicationPort
          ToPort: !Ref ApplicationPort
          SourceSecurityGroupId:
            Fn::ImportValue: course-project-alb-security-group
      Tags:
        - Key: Name
          Value: !Sub ${ServiceName}-containers
        - Key: Project
          Value: Course Project

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      # Name of the task definition. Subsequent versions of the task definition are grouped together under this name.
      Family: !Sub ${ServiceName}-course-project
      # "The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that grants containers in the task permission to call AWS APIs on your behalf."
      TaskRoleArn:
        Fn::ImportValue: course-project-ecs-service-role
      ExecutionRoleArn:
        Fn::ImportValue: course-project-ecs-task-execution-role
      # awsvpc is required for Fargate
      NetworkMode: bridge
      RequiresCompatibilities:
          - EC2
      Cpu: !Ref EcsTaskCpuLimit
      Memory: !Ref EcsTaskMemoryLimit
      # A role needed by ECS.
      # "The ARN of the task execution role that containers in this task can assume. All containers in this task are granted the permissions that are specified in this role."
      # "There is an optional task execution IAM role that you can specify with Fargate to allow your Fargate tasks to make API calls to Amazon ECR."
      ContainerDefinitions:
        - Name: !Ref ServiceName
          Image: !Sub ${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${ServiceName}:${ImageTag}
          PortMappings:
            - HostPort: 0
              Protocol: tcp
              ContainerPort: !Ref ApplicationPort
          # Send logs to CloudWatch Logs
          Essential: true
          Environment:
            - Name: MYSQL_HOSTNAME
              Value:
                Fn::ImportValue: "course-project-db-url"
            - Name: MYSQL_PORT
              Value: !Ref MySqlPort
            - Name: MYSQL_DATABASE
              Value: !Ref MySqlDb
            - Name: LISTEN_PORT
              Value: !Ref ApplicationPort
            - Name: SECURITY_LEVEL
              Value: !Ref SecurityLevel
          Secrets:
            - Name: MYSQL_USERNAME
              ValueFrom: !Sub ${ServiceName}-db-username
            - Name: MYSQL_PASSWORD
              ValueFrom: !Sub ${ServiceName}-db-password
            # - Name: RECAPTCHA_PRIV_KEY
              # ValueFrom: !Sub ${ServiceName}-recapcha-priv-key
            # - Name: RECAPTCHA_PUB_KEY
              # ValueFrom: !Sub ${ServiceName}-recapcha-pub-key

          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref LogGroup
              awslogs-stream-prefix: ecs
      Tags:
        - Key: Project
          Value: Course Project

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 15
      # will look for a 200 status code by default unless specified otherwise
      HealthCheckPath: "/login.php"
      HealthCheckProtocol: "HTTP"
      HealthCheckTimeoutSeconds: 5
      UnhealthyThresholdCount: 2
      HealthyThresholdCount: 2
      Name: !Sub ${ServiceName}-ec2-tg
      Port: 80
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: "60" # default is 300
      Matcher:
        HttpCode: '200-399'
      TargetType: instance
      VpcId:
        Fn::ImportValue: course-project-vpc

  ServiceEC2:
    Type: AWS::ECS::Service
    Properties:
      Cluster:
          Fn::ImportValue: course-project-cluster-logical-id
      ServiceName: !Sub ${ServiceName}-ec2
      DesiredCount: !Ref DesiredCount
      LoadBalancers:
        -
          ContainerName: !Ref ServiceName
          ContainerPort: !Ref ApplicationPort
          TargetGroupArn: !Ref TargetGroup
      TaskDefinition: !Ref TaskDefinition
      PlacementStrategies:
        - Field: "attribute:ecs.availability-zone"
          Type: "spread"
        - Field: "instanceId"
          Type: "spread"
      HealthCheckGracePeriodSeconds: 15
      Tags:
        - Key: Project
          Value: Course Project

  albListenersRulePort80:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
        - Type: "redirect"
          RedirectConfig:
            Protocol: "HTTPS"
            Port: "443"
            Host: "#{host}"
            Path: "/#{path}"
            Query: "#{query}"
            StatusCode: "HTTP_301"
      Conditions:
      - Field: host-header
        Values:
          - !Sub "${SubDomainName}.${RootDomainName}"
      ListenerArn:
        Fn::ImportValue: course-project-alb-listener-port-80
      Priority: 2

  albListenersRulePort443:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
      - Type: forward
        TargetGroupArn: !Ref TargetGroup
      Conditions:
      - Field: host-header
        Values:
          - !Sub "${SubDomainName}.${RootDomainName}"
      ListenerArn:
        Fn::ImportValue: course-project-alb-listener-port-443
      Priority: 2

  AutoScalingTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    Properties:
      MaxCapacity: !Ref MaxCapacity
      MinCapacity: !Ref MinCapacity
      ResourceId:
        Fn::Sub:
          - service/${ClusterName}/${Service}
          - {ClusterName: !ImportValue course-project-cluster-logical-id, Service: !GetAtt ServiceEC2.Name}
      RoleARN:
        Fn::ImportValue: course-project-ecs-service-autoscaling-role
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs

  ScaleUpPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Sub "${ServiceName}-scale-up-policy"
      PolicyType: StepScaling
      ScalingTargetId: !Ref AutoScalingTarget
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      StepScalingPolicyConfiguration:
        AdjustmentType: ChangeInCapacity
        Cooldown: 60
        MetricAggregationType: Average
        StepAdjustments:
          - MetricIntervalLowerBound: 0
            ScalingAdjustment: 1

  ScaleDownPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Sub "${ServiceName}-scale-down-policy"
      PolicyType: StepScaling
      ScalingTargetId: !Ref AutoScalingTarget
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      StepScalingPolicyConfiguration:
        AdjustmentType: ChangeInCapacity
        Cooldown: 60
        MetricAggregationType: Average
        StepAdjustments:
          - MetricIntervalLowerBound: 0
            ScalingAdjustment: -1

  ScaleUpCpuAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: !Sub "Scale-up if CPU > ${ScaleUpCpuThreshold}% for 5 minutes"
      AlarmName: !Sub "${ServiceName}-service-scaleup-cpu-alarm"
      EvaluationPeriods: 5
      Period: 60
      Threshold: !Ref ScaleUpCpuThreshold
      Statistic: Average
      MetricName: CPUUtilization
      Namespace: AWS/ECS
      AlarmActions:
        - !Ref ScaleUpPolicy
      ComparisonOperator: GreaterThanOrEqualToThreshold
      Dimensions:
        - Name: ServiceName
          Value: Fn::GetAtt ServiceName
        - Name: ClusterName
          Value: !ImportValue course-project-cluster-logical-id

  ScaleDownCpuAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmDescription: !Sub "Scale-up if CPU > ${ScaleDownCpuThreshold}% for 5 minutes"
      AlarmName: !Sub "${ServiceName}-service-scaledown-cpu-alarm"
      EvaluationPeriods: 5
      Period: 60
      Threshold: !Ref ScaleDownCpuThreshold
      Statistic: Average
      MetricName: CPUUtilization
      Namespace: AWS/ECS
      AlarmActions:
        - !Ref ScaleDownPolicy
      ComparisonOperator: GreaterThanOrEqualToThreshold
      Dimensions:
        - Name: ServiceName
          Value: Fn::GetAtt ServiceName
        - Name: ClusterName
          Value: !ImportValue course-project-cluster-logical-id

  DNSRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      AliasTarget:
        DNSName:
          Fn::ImportValue: course-project-alb-dns-name
        HostedZoneId:
          Fn::ImportValue: course-project-alb-canonical-hosted-zone-id
      HostedZoneName: !Sub "${RootDomainName}."
      Name: !Sub "${SubDomainName}.${RootDomainName}"
      Type: A

  taskEssentialExitEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Name: !Sub "${ServiceName}-ecs-task-essential-exit-rule"
      Description: "Rule which keeps record of task exit events"
      EventPattern: !Sub
        - |
            {
              "source": [
                "aws.ecs"
              ],
              "detail-type": [
                "ECS Task State Change"
              ],
              "detail": {
                "clusterArn": ["arn:aws:ecs:${AWS::Region}:${AWS::AccountId}:cluster/${ecsCluster}"],
                "taskDefinitionArn": ["${TaskDefinition}"],
                "stoppedReason": ["Essential container in task exited"]
              }
            }
        - ecsCluster: 
            Fn::ImportValue: course-project-cluster-logical-id
      State: ENABLED

  taskEssentialExitAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub "${ServiceName}-ecs-task-essential-exit-alarm"
      EvaluationPeriods: 1
      Statistic: Sum
      Threshold: 2
      AlarmDescription: Alarm if task exited 2 times per minute
      Period: 60
      Namespace: AWS/Events
      Dimensions:
      - Name: RuleName
        Value: !Ref taskEssentialExitEventRule
      ComparisonOperator: GreaterThanOrEqualToThreshold
      MetricName: TriggeredRules

  taskFailedHealthCheckEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Name: !Sub "${ServiceName}-ecs-failed-healthcheck-rule"
      Description: "Rule which keeps record of task exit events"
      EventPattern: !Sub
        - |
            {
              "source": [
                "aws.ecs"
              ],
              "detail-type": [
                "ECS Task State Change"
              ],
              "detail": {
                "clusterArn": ["arn:aws:ecs:${AWS::Region}:${AWS::AccountId}:cluster/${ecsCluster}"],
                "taskDefinitionArn": ["${TaskDefinition}"],
                "stoppedReason": ["Task failed ELB health checks in (target-group ${targetGroupLbArns})"]
              }
            }
        - ecsCluster: 
            Fn::ImportValue: course-project-cluster-logical-id
          targetGroupLbArns:
            !Ref TargetGroup
      State: ENABLED

  taskFailedHealthCheckAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmName: !Sub "${ServiceName}-ecs-failed-healthcheck-fail-alarm"
      EvaluationPeriods: 1
      Statistic: Sum
      Threshold: 2
      AlarmDescription: Alarm if task healt check failed 2 times per minute
      Period: 60
      Namespace: AWS/Events
      Dimensions:
      - Name: RuleName
        Value: !Ref taskFailedHealthCheckEventRule
      ComparisonOperator: GreaterThanOrEqualToThreshold
      MetricName: TriggeredRules

Outputs:
  ServiceDNSRecord:
    Value: !Ref DNSRecord
  taskEssentialExitAlarm:
    Value: !GetAtt taskEssentialExitAlarm.Arn
  taskFailedHealthCheckAlarm:
    Value: !GetAtt taskFailedHealthCheckAlarm.Arn